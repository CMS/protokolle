# Chaos macht Schule Treffen

**Date:** Donnerstag, 09.11.2023, ab 16 Uhr
**Ort:** [c3d2/hq/proc](https://c3d2.de/space.html)

- [vorheriges Treffen 19.10.2023](https://gitea.c3d2.de/CMS/protokolle/src/branch/master/2023_10_19.md)
- [dieses Treffen 09.11.2023](https://hedgedoc.c3d2.de/VbF3ELa3RNKg8T8UNwnC_w)
- [nächstes Treffen 13.12.2023](https://hedgedoc.c3d2.de/iCbTlWlsTwCY8giSn44hIg#)

---
[TOC]

---

## Offene Todos
- [ ] Visitenkarte: QR-Code Link menschenlesbar integrieren
- [ ] Visitenkarte: Format anpassen (Größe EC-Karte messen und übernehmen)
- [ ] Flyer: QR Code mit integriertem c3d2 Logo einbauen
- [ ] Pads: Links anpassen mit neuem Format
- [ ] Website: Neuen Entwurf hochladen
- [ ] Website: Termin nochmal anpassen, falls alternierender Termin festgelegt wird
- [ ] Website: Neues Repo für neue Quellen/Materialien eröffnen
- [ ] Website: Altes Repo auf private stellen, da Lizenzbedenken aufgekommen sind (einige Links zu Bildern fehlen)
- [ ] Website News veröffentlichen
- [ ] Pad hier: Bilder von Visitenkarte + erstem Flyerentwurf einfügen
- [ ] Just for fun: Unterschiedliche "Hello, Worlds!" in Website einbauen
- [ ] Folien bauen

## Anfragen? / Gäste?
- keine neuen

### Wollen c3d2 CmS Menschen was auf dem Congress tun?
> Hallo miteinanander,
>
> auf dem Congress wird es aller Voraussicht nach einen Junghackertag geben, ein Orga-Team hat sich dafür gefunden.
Nun die Frage in die Runde:
>
> Wollen wir als CMS ein, zwei Workshops dazu beitragen? Anbieten würde sich wieder ein Lötworkshop, da sind wir erprobt, weitere Ideen sind willkommen.
> 
> Bitte gebt mir mal kurz Rückmeldung, wenn Ihr was mit beitragen wollt. Dann kann ich den Stand ins Orga-Team hineingeben, das trifft sich Samstag wieder.
> 
> Ich wäre dabei, wenn Ihr dabei seid :)
Wenn wir eine Truppe zusammen bekommen, sollten wir zeitnah einen Vikotermin finden, um in die Planung zu gehen.
> 
> Und sorry für cross-posting.

Kam auf der Bundes-CmS-Liste rum

- Speziell gerade niemand... eher Spontan

## Newsankündigung auf c3d2.de für das offene CMS Treffen
[Chaos macht Schule](https://c3d2.de/schule.html), kurz CMS, ist ein Projekt, das die Förderung von Medienkompetenz und Technikverständnis in Schulen zum Ziel hat. 

Wir laden alle interessierten Lebewesen zu unserem offenen Treffen im [hq des c3d2](https://c3d2.de/space.html) ein. Zum CMS Treffen freuen wir uns über offenen Diskurs und besprechen, meist bei einer veganen Mahlzeit, aktuelle Anfragen an uns, bereiten entsprechende Materialien vor und stehen jederzeit für Fragen zur Verfügung.

Du bist bei uns richtig wenn...
- du Menschen technische Sachverhalte vermitteln möchtest und Unterstützung brauchst,
- du Bildungslücken in Bezug auf aktuelle Technologieentwicklungen siehst und nicht weißt, wie man sie schließen kann,
- oder dir digitale Selbstverteidigung, freie Software, Medienkompetenz und Spaß an Technik wichtig sind.

Komm' vorbei!

## Input TUD Lehramt (nac, kilian, benny)
- Was ist der c3d2/cms?
- Example Talk
    - ggf. den Studierenden die Auswahl lassen?
    - ja...
    - potentielle Themenideen
        - KI in der Bildung
            - Kiste der Pandora ist offen, was jetzt?
            - Werkzeug vs. Gefahren
        - Identität
            - Was für Spuren hinterlässt man?
            - Auswirkungen von Microtargeting, Werbung, etc.
        - https://www.swr.de/swraktuell/rheinland-pfalz/koblenz/lehrerin-kinderpornografischer-inhalte-konfisziert-deswegen-angeklagt-100.html

## Elternsprecheranfrage?
- Anfrage kam von Elternvertreterin einer 6. Klasse
- es soll im Elternrat besprochen werden, ggf. eine Kooperation zu etablieren (vermutlich regelmäßige Workshops)
- Hauptthema wäre Datenschutz und soziale Netzwerke (...da die Kinder schon sehr intensiv das Smartphone nutzen...)
- Bisher kein Feedback, vesala fragt nochmal nach

## Physisches Info-Material zum Verteilen
- Idee den Link zur Seite ueber Sticker zu verteilen (+ QR-Code?)
    - aviac war so lieb und hat Visitenkarten erstellt
    - Flyer ist noch in Entwicklung... danke!
- Flyer, Visitenkarten drucken lassen? -> Abstimmung Visitenkarte
    - ob wir wollen spielt keine Rolle mehr... es ist da, getreu dem Motto "Wer macht hat recht!"

## Fortschritt Update der Webseite
- [x] Fix fuer missing logo
- [x] Link zu [Mothership-CMS](https://www.ccc.de/de/cms-forderungen-lang) integrieren, da dort eine ordentliche Erklaerung/Definition von CMS existiert
  - damit ersparen wir uns die Arbeit uns eine neue Beschreibung aus der Nase zu ziehen
  - ausserdem koennen Leute schneller und unabhaengig von uns check, ob sie mit unseren Werten etc uebereinstimmen
  - vielleicht sollten wir trotzdem noch erwaehnen, dass es jetzt keine heiligen, super strikten Gesetze sind sondern eher Richtlinien (?)
- [x] [Update allgemeiner Website Text PR zusammen überarbeiten](https://gitea.c3d2.de/c3d2/c3d2-web/pulls/4)
    - [siehe auch Hedgedoc Entwurf](https://hedgedoc.c3d2.de/YBQInVmjRwiTiujW9jGLyg#)

## Neues Template für Folien
- Auf Basis von Hedgedoc Basisfolien erstellen.
- Template enthält Einführung.

## Versuch aus vorhandenen Folien Themen zu extrahieren
- Ähnlich zu den den Basisfolien könnte man Module bauen, die eine Basis für verschiedene Themen bildet auf der man aufbauen kann.

## NMBD
**Treffen am 06.12.2024**

> Liebes Netzwerkmitglieder, liebe Interessierte,
>  
> In knapp einem Monat ist es soweit! Hiermit möchte ich euch gern zu unserem 2. Großen Netzwerktreffen Medienbildung Dresden, am 6.12.2023, im Kraftwerk Mitte 3, im Veranstaltungsraum EG einladen.
Hier die Tagesordnung:
9:30 Uhr – 12 Uhr Projektauswertung – und Planung CrossMedia Tour und Code Week
Rückblick CrossMedia Tour und Code Week 2023
Evaluationsbericht
Öffentlichkeitsarbeit
Bedarfe und Potentiale aus dem Netzwerk
Ausblick CrossMedia Tour und Code Week 202
Finanzierung, Personalaufstellung
Netzwerkarbeit
Kooperation Medienkulturzentrum/CrossMedia Tour e.V.
Sammlung Planungsstand Veranstaltungen 2024 aus dem Netzwerk
12:00 - 12.45 Uhr Mittagessen (auf Spendenbasis)
12:45 – 13.45 offenes Netzwerktreffen (Mitglieder aus NMBD, CMT, Code Week, Interessierte)
Neuigkeiten aus anderen Netzwerken bundesweit/sachsenweit – Input aus dem Netzwerk
Rückschau 1. Netzwerktreffen 2023 - wie geht es mit den Ergebnissen weiter
News aus dem Netzwerk
Bei Bedarf noch weitere Themen möglich (z.B: neue Projektideen und Kooperationswünsche, NMBD -  wie soll es damit weiter gehen, etc)
14 - 15 Uhr Einblicke in die medienpädagogische Praxis (Angebote finden parallel statt)
Programmieren mit den Ozobots – Sebastian Knappe, vom Bündnis Medienkulturzentrum Dresden und Städtische Bibliotheken Dresden
Nexus Drive - Mario-Kart-Rennstrecke im Kraftwerk Mitte – Kris und Alex von der Nexus- Initiative
Selber Trickfilm machen – Mandy Müller vom Fantasia Dresden
Damit wir für den Tag planen können (und das Mittagessen bestellen können), ist es für uns wichtig, dass ihr euch bis zum 24.11.2023 in folgende Anmeldeliste eintragt.
Bitte meldet euch außerdem bei uns zurück, wenn ihr:
noch Ergänzungen und Wünsche zur Tagesordnung habt
ihr selber gern Einblick in eure Praxis geben möchtet
ihr selber noch ein Thema setzen möchtet
Wir freuen uns auf Ideenentwicklung, Austausch mit euch und die Planung für 2024!
Viele Grüße,

## Pad Url
- [name=t3sserakt] Können wir uns auf ein Format für die Pad Url einigen.
Vorschlag: cms-YYMMDD Beispiel: cms-231109
- Gibt es ein Veto?
    - nein beteiligte Lebewesen finden die Idee gut.
    - Welches Format?
        - ```cms-YYYYMMTT```

## Termin
- Überlegungen zu alternierenden Terminen. Bitte hier Vorschläge drunter von allen, die öfters nicht Mittwoch können:
    - ja, wir brauchen noch Varianten/Termine

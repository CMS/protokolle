# Chaos macht Schule Treffen

* **Zeit:** [time=09 MM YYYY HH:MM] 
* **Ort:** [C3D2 HQ](https://c3d2.de/space.html), ist auch barrierearm
* **Pad:** https://hedgedoc.c3d2.de/CMS-<YYYY-MM-DD>#
* **nächstes Treffen** [YYYY-MM-DD  ab HH:MM Uhr](https://hedgedoc.c3d2.de/CMS-<YYYY-MM-DD>#)
* [Ältere Protokolle](https://gitea.c3d2.de/CMS/protokolle)

[TOC]

# TOP 1

# TOP 2
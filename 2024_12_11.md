# offenes Chaos macht Schule Treffen

**11.12.2024 (17 Uhr)**
**Ort:** Zentralbibliothek


- [vorheriges Treffen 28.11.2024 (17 Uhr, ZentralWerk)](https://hedgedoc.c3d2.de/CMS-2024-11-28)
- [diesesTreffen 11.12.2024 (17 Uhr, Zentralbibliothek)](https://hedgedoc.c3d2.de/CMS-2024-12-11)
- [nächstes Treffen 19.12.2024 (17 Uhr, c3d2/hq/proc)](https://hedgedoc.c3d2.de/CMS-2024-12-19)

## Vorstellungsrunde?
- ja

## Gäste?
- 2

## Fragen
- Sicherheitsaspekte
    - 2 Faktor authentifizierung
    - Passwort Manager
    - Adblock 
- Kommunikation
    - Wie nutze ich digitale Dinge um mit Bekannten und Freunden zu kommunizieren.

## Sonstiges
- eine Frau der Bibliothek erkundigte sich wie es so läuft [name=nac] gab ein kurzes, positives Feedback

- Welche Angebote für Menschen in Dresden gibt es?
    - https://www.medienkulturzentrum.de/seminar/digitale-kompetenz-fuer-seniorinnen/
    - Seniorenakademie im Hygienemuseum


Empfehlungen für Menschen
- DNS-Server von Fritzbox auf freien Server einstellen
    - Sonst Server von Provider
    - Netzsperren
    - z.B. dnsforge.de
    - oder der von digitalcourage
- Freifunk gibt dir einen Router
    - Du teilst Internetleitung
    - Verkehr geht über VPN
- Fritzbox VPN




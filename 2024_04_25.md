# offenes Chaos macht Schule Treffen

**25.04.2024 (17 Uhr)**
**Ort:** c3d2/proc/hq

- [vorheriges Treffen 10.04.2024 (18 Uhr)](https://hedgedoc.c3d2.de/CMS-2024-03-28)
- [dieses Treffen 25.04.2024 (17 Uhr)](https://hedgedoc.c3d2.de/CMS-2024-04-10)
- [nächstes Treffen 08.05.2024 (1? Uhr)](https://hedgedoc.c3d2.de/CMS-2024-05-08)

---

[TOC]

---

## Gäste
- für die Bibliotheken ist Jana dabei

## Anfragen?
- keine auf der Mailingliste

## Themen

### Bibliothek Dresden (Jana)
- Jana stellt sich kurz vor
    - Medienpädagogin der LHD Bibliotheken
    - für Kinder, Jugendliche, Familien, Senioren
    - Schulisch wie auch außerschulisch
    - ab 1.5. neue Medienpädagogin in einer Zweigstelle Prohlis, Süd
    - Perspektivisch in jede Himmelsrichtung eine Medienpädagogin
    - Fahrbibliothek soll umgebaut werden zum Makerspace
    - haben 30 iPads, und diverse weitere Dinge (Konsolen, Roboter, VR-Brillen, etc.)
- wir stellen uns für Jana (und nac) kurz vor
- Was können wir gemeinsam machen?
    - Stuttgart hat schon Kooperation mit lokalem CCC
    - regelmäßige Veranstaltungen zu unterschiedlichen Themen
    - Beratung, Repair-Café, Vorträge, Workshops
    - Ist das mit (geringem) Aufwand auch hier möglich?
    - Roboter Workshop
    - Wie erkenne ich Fakenews?
    - Zielgruppe: Senioren
    - aktuell werden noch Themen gesammelt
    - Vorschlag: offenes CMS Treffen in Bibliotheken in Dresden.
    - Foyer (ganz oben), als normales Treffen --> ganz offenes Treffen
    - Wie die Leute "ansprechen"?
    - Zeit (Vorschlag): an einem Mittwoch im Monat
    - Start ab August/September, nach den Sommerferien
        - Wir können aber auch einfach früher starten und einfach das offene Treffen in die Zentralbibliothek verlagern
    - Terminvorschläge: 08.05., 12.06., 10.07., 14.08., 11.09., 09.10., 13.11., 11.12. jeweils 17-19h

## ToDo
- Jana braucht bis Montag, 29.04.
    - Titel
    - Beschreibungstext
    - Teaser
    - logo(s)
    - Link zum Protokoll

## Workshop/Situation @ Schule von Gruppenmitglied


## Laufende Themen
- NMBD
- Bildung_21

---

## Recherche
- Können wir bitte hier noch kurz etwas Kontext dazuschreiben?

https://hedgedoc.c3d2.de/CMS-2024-Recherche#

---

## Text für die Bewerbung bei den Bibos

Du interessierst dich für Technik, Internet, Datenschutz oder KI und möchtest mehr darüber erfahren? Dann bist du bei den offenen Treffen von "Chaos macht Schule" genau richtig! Wir vom Chaos Computer Club Dresden erklären dir in lockerer Runde alles rund um diese spannenden Themen.
Egal ob du Schüler·in, Elternteil, Lehrer·in oder einfach nur neugierig bist - jeder ist willkommen! Wir zeigen dir, wie man Technik sinnvoll nutzt und worauf man achten sollte.
Die Treffen finden ab sofort regelmäßig in Kooperation mit den Städtischen Bibliotheken Dresden statt. Schau einfach vorbei, stell deine Fragen und lerne gemeinsam mit anderen mehr über die digitale Welt.
Wir freuen uns auf dich!

## offene Punkte

- MineTest Abend Kinder/Eltern Idee nochmal als Gruppe besprechen
- Büchertausch aviac, toximas

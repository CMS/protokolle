# offenes Chaos macht Schule Treffen

**10.04.2024 (18 Uhr)**
**Ort:** c3d2/proc/hq

- [vorheriges Treffen 28.03.2024 (18 Uhr)](https://hedgedoc.c3d2.de/CMS-2024-03-28)
- [dieses Treffen 10.04.2024 (18 Uhr)](https://hedgedoc.c3d2.de/CMS-2024-04-10)
- [nächstes Treffen 25.04.2024 (17 Uhr)](https://hedgedoc.c3d2.de/CMS-2024-04-25)
 
---

[TOC]

---

## Gäste?
- Die Slub ist verhindert
- 2 Gäste zum Thema KI
  - wir stellen uns und was wir machen vor
  - Feedback zur website: 
      - schwer zu navigieren
      - der Inhalt scheint sehr unübersichtlich
  - Benny erklärt was KI, ChatGPT, etc. ist

## Anfragen?
- keine auf der Mailingliste

## Themen
- KI
  - Wir erklären den Gästen KI und auf was aufgepasst werden muss
- NMBD

## Workshop/Situation @ Schule von Gruppenmitglied
- Gespräch über event um Vertrauen zwischen Kindern und Leuten an die sie sich wenden können wenn es digitale "Vorfälle" jeglicher Art gibt (z.B. Mobbing)
- Ansprechpersonen: Eltern, Info Lehrer, etc.
- Idee: Spieleabend um im digitalen Rahmen spielerisch ein paar Dinge zu vermitteln aus dem >CYBERSPACE<
- Wir haben ein paar neue Ressourcen gesammelt die aviac privat in seine Sammlung aufnimmt
- Wir hatten die Idee diese Ressourcen als Gruppe mal zur Verfügung zu stellen
- Ausserdem sollten wir mal eine Liste mit Ansprechpartner:innen für schlimme Cybermobbing/Missbrauchsfälle online stellen auf die wir einfach verweisen können

## ToDo von letztem Male
- Planung für das kommende Treffen
- Dieses Dokument zu einem Template für zukünftige Treffen ausarbeiten

## Laufende Themen
- NMBD
- Bildung_21

---

## Recherche
- Können wir bitte hier noch kurz etwas Kontext dazuschreiben?

https://hedgedoc.c3d2.de/CMS-2024-Recherche#
